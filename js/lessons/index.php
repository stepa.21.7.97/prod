<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
   <link rel="stylesheet" href="/prod/main-style.css">
</head>
<body>
    <header>
        
            <h1>Кароче чел. Это что то в роде забавы ради, а что то в роде моей библиотеки</h1>
            <h3>Сюда я добавляю все то что наработал, выучил, решил записать</h3>
        
    </header>
    <div id="menu-block">
        <ul>
            <li> <a href="/prod/git-learn/"> все про гит</a> </li>
            <li> <a href="/prod/js/">обучалка и практика по JavaScript</a></li>
            <li> <a href="/prod/php/"> обучалка и практика по PHP</a></li>
            <li> <a href="/prod/sql/"> обучалка и практика по SQL</a></li>
            <li> <a href="/practic_work/">Решение некоторых задач, которые я посчитал трудными </a></li>
        </ul>


        
    </div>
    <div id="content">
       <ul>
        <li><a href="/prod/js/lessons/1-console-log/">Урок 1 приветстви в консоли</a></li>
        <li><a href="/prod/js/lessons/2/">Прописываем котов</a></li>
        <li><a href="/prod/js/lessons/3/">Типы данных и переменные</a></li>
        <li><a href="/prod/js/lessons/4/">Строковый тип данных</a></li>
        <li><a href="/prod/js/lessons/5/">Boolean Логичиский тип данных и чуть чуть про NULL и undefined</a></li>
        <li><a href="/prod/js/lessons/6/">Массивы</a></li>
        <li><a href="/prod/js/lessons/7/">Случайные числа и округление чисел</a></li>
        <li><a href="/prod/js/lessons/8/">Объекты. Сложный тип данных</a></li>
       </ul>
        
    </div>





    <footer>
        <div>
            это футер
            <br>
            
        </div>
    </footer>
    
    
    
</body>
</html>