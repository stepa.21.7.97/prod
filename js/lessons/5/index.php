<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="/prod/style.css">
</head>
<body>
    <div class="content">
        <h2>Boolean Логический тип данных, NULL undefined</h2>
        В логический тип данных входит значения true(он же равен 1) и false(он же равен 0). 
        <br>
        Эти значения нужны для того чтобы управлять логическими операторами и получать нужные нам значения 
        <br>
        <h3>Логические операторы</h3>
        <ul>
            <li> <b> true && true  - оператор двойной амперсент(он же &&) обозначает "и"</b>. То есть операнды все были истина, если хоть один операнд не равно истина то и значение будет ложь
            <br>   
                <ul>
                <li>true && true = true</li>
                <li>true && false = false</li>
                <li>false && true = false</li>
                <li>false && false = false</li>
               </ul>
            </li>
            <li> <b> true || true - оператор "или"(он же ||).</b> То есть если хоть один операнд будет истина то значение будет истина
                <br>
                <ul>
                    <li>true || true = true</li>
                    <li>true || false = true</li>
                    <li>false || true = true</li>
                    <li>false || false = false</li>
                </ul>

            </li>
            <li><b>!true=false - оператор "не равно" (он же !).</b> То есть если операнд не равен истина то будет ложь
                <br>
                <ul>
                    <li>!true = false</li>
                    <li>!false = true</li>
                </ul>
            </li>
        </ul>
        <br>
        Так же есть простые проверки на истину или ложь. Это знаки боль меньше или равно, двойной и тройной знак равно. В случае двойного знака то он проверяет на равенство переменной, а 3 равно проверяет не только равенство переменной но и тип переменной. 
        <br>
        "5"==5;
        <br>
        true;
        "5"===5;
        <br>
        false;("5" - значение 5 тип данных string, 5 - значение 5 тип данных number, по этому и false)
        <br>
        var age = 11; 
        var accompained = true;
        age >=12 || acoompained === true; 
        консоль покажет истина, но если accompained = false то консоль выведет ложь. 
        <br>
        Так же есть значения undefined и null. undefined это когда в переменной нет ничего(var a = ;). Null похожая на undefined но разница в том, что нулл в переменную специально записывается и к сожалению эти 2 типа данных не одинаковы хоть и значения похожи. 
        
  </div>
</body>
</html>